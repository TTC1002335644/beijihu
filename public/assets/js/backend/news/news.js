define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'news/news/index' + location.search,
                    add_url: 'news/news/add' + location.search,
                    edit_url: 'news/news/edit',
                    del_url: 'news/news/del',
                    multi_url: 'news/news/multi',
                    table: 'news',
                }
            });

            var table = $("#table");
            $(".btn-add").data("area", ["100%", "100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'news_id',
                sortName: 'news_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'news_id', title: __('News_id')},
                        // {field: 'language_id', title: __('Language_id')},
                        {field: 'news_category_id', title: __('News_category_id')},
                        {field: 'title', title: __('Title')},
                        {field: 'image', title: __('Image'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'author', title: __('Author')},
                        {field: 'is_open', title: __('Is_open'), searchList: {"1":__('Is_open 1'),"2":__('Is_open 2')}, formatter: Table.api.formatter.normal},
                        {field: 'is_external', title: __('Is_external'), searchList: {"1":__('Is_external 1'),"2":__('Is_external 2')}, formatter: Table.api.formatter.normal},
                        {field: 'external_url', title: __('External_url'), formatter: Table.api.formatter.url},
                        {field: 'showtime', title: __('Showtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});