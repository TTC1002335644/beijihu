define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'album/album_description/index' + location.search,
                    add_url: 'album/album_description/add',
                    edit_url: 'album/album_description/edit',
                    del_url: 'album/album_description/del',
                    multi_url: 'album/album_description/multi',
                    table: 'album_description',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'description_id',
                sortName: 'description_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'description_id', title: __('Description_id')},
                        {field: 'album_id', title: __('Album_id')},
                        {field: 'seo_title', title: __('Seo_title')},
                        {field: 'seo_keywork', title: __('Seo_keywork')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});