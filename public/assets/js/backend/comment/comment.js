define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'comment/comment/index' + location.search,
                    add_url: 'comment/comment/add',
                    edit_url: 'comment/comment/edit',
                    del_url: 'comment/comment/del',
                    multi_url: 'comment/comment/multi',
                    table: 'comment',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'comment',
                sortName: 'comment',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'comment', title: __('Comment')},
                        {field: 'language_id', title: __('Language_id')},
                        {field: 'name', title: __('Name')},
                        {field: 'tel', title: __('Tel')},
                        {field: 'email', title: __('Email')},
                        {field: 'qq', title: __('Qq')},
                        {field: 'title', title: __('Title')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});