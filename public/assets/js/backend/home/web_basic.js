define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'home/web_basic/index' + location.search,
                    add_url: 'home/web_basic/add',
                    edit_url: 'home/web_basic/edit',
                    del_url: 'home/web_basic/del',
                    multi_url: 'home/web_basic/multi',
                    table: 'web_basic',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'logo', title: __('Logo')},
                        {field: 'favicon', title: __('Favicon'), formatter: Table.api.formatter.icon},
                        {field: 'record_number', title: __('Record_number')},
                        {field: 'record_url', title: __('Record_url'), formatter: Table.api.formatter.url},
                        {field: 'contact', title: __('Contact')},
                        {field: 'telphone', title: __('Telphone')},
                        {field: 'phone', title: __('Phone')},
                        {field: 'mail', title: __('Mail')},
                        {field: 'address', title: __('Address')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});