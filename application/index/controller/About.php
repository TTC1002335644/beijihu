<?php
namespace app\index\controller;


use think\Controller;
use app\admin\model\home\About as AboutModel;

class About extends Controller{


    public function getAbout(int $language_id = 0){
        $res = db('about')->where(['language_id' => $language_id])->find();
        return $res;
    }

}