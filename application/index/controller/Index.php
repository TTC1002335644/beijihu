<?php

namespace app\index\controller;

//use app\common\controller\Frontend;

use app\admin\model\news\News as NewsModel;

class Index extends Base {

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function index(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

    /**
     * 新闻版面
     * @param int $pageSize
     * @param int $pageNow
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function article(int $pageSize = 10 , int $pageNow = 1){
        $condition = [
            'language_id' => $this->paramLanguageId,
            'is_open' => NewsModel::IS_OPEN_TRUE
        ];
        $newsList = (new NewsModel())
            ->where($condition)
            ->paginate($pageSize);
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);

        $this->view->assign('bannerList' , $bannerList);
        $this->view->assign('newsList' , $newsList);
        return $this->view->fetch();
    }

    /**
     * 新闻详情
     * @param int $news_id
     * @return string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function article_detail(int $news_id = 0)
    {
        $newsDetail = (new NewsModel())
            ->where(['news_id' => $news_id , 'is_open' => NewsModel::IS_OPEN_TRUE])
            ->with([
                'hasOneNewsDescription'
            ])
            ->find();

        /**外链直接跳转**/
        if(!empty($newsDetail) && $newsDetail->is_external == NewsModel::IS_EXTERNAL_TRUE){
            header('Location: '.$newsDetail->external_url);
            exit();
        }

        /**没有这篇文章的时候，跳转到新闻列表**/
        if(empty($newsDetail)){
            header('Location: /news.html?lang='.$this->paramLanguageCode);
            exit();
        }

        /**获取上下篇**/
        $prveNext = (new News())->getPrevNextNews($news_id , $this->paramLanguageId , $this->paramLanguageCode);


        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);

        $this->view->assign('newsDetail' , $newsDetail);
        $this->view->assign('prveNext' , $prveNext);
        return $this->view->fetch();
    }

    /**
     * 关于我们（公司简介）
     * @return string
     * @throws \think\Exception
     */
    public function about(){
        $aboutRes = (new About())->getAbout($this->paramLanguageId);

        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        $this->view->assign('about' , $aboutRes);
        return $this->view->fetch();
    }

    /**
     * 成功案例
     * @return string
     * @throws \think\Exception
     */
    public function album(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

    /**
     * 案例详情
     * @return string
     * @throws \think\Exception
     */
    public function album_detail(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

    /**
     * 联系我们
     * @return string
     * @throws \think\Exception
     */
    public function contact(){
        $contact = (new Contact())->getDetail($this->paramLanguageId);
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        $this->view->assign('contact' , $contact);
        return $this->view->fetch();
    }

    public function product(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

    public function product_detail(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }


    /**
     * 基地列表
     * @return string
     * @throws \think\Exception
     */
    public function bases(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

    /**
     * 基地详情
     * @return string
     * @throws \think\Exception
     */
    public function bases_detail(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

    /**
     * 在线留言
     * @return string
     * @throws \think\Exception
     */
    public function comment(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

}
