<?php
namespace app\index\controller;

use think\Controller;
use app\admin\model\news\News as NewsModel;

class News extends Controller{

    /**
     * 获取上一张和下一张
     * @param int $news_id
     * @param int $language_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getPrevNextNews(int $news_id = 0 , int $language_id = 1 , string $lang){
        $condition = [ 'language_id' => $language_id , 'is_open' => NewsModel::IS_OPEN_TRUE];
        $field = ['news_id' , 'title' , 'showtime' ,'is_external' , 'external_url'];
        $prevData = (new NewsModel)->where($condition)->where('news_id' , '<' , $news_id)->order('news_id','desc')->field($field)->find();
        $nextData = (new NewsModel)->where($condition)->where('news_id' , '>' , $news_id)->order('news_id','asc')->field($field)->find();
        if(!empty($prevData)){
            $prev = [
                'title' => $prevData->title,
                'url' => ($prevData->is_external == NewsModel::IS_OPEN_TRUE) ? $prevData->external_url :'./news_detail.html?lang='.$lang.'&news_id='.$prevData->news_id,
            ];
        }else{
            $prev = [
                'title' => __('None'),
                'url' => '#',
            ];
        }

        if(!empty($nextData)){
            $next = [
                'title' => $nextData->title,
                'url' => ($nextData->is_external == NewsModel::IS_OPEN_TRUE) ? $nextData->external_url : './news_detail.html?lang='.$lang.'&news_id='.$nextData->news_id,
            ];
        }else{
            $next = [
                'title' =>__('None'),
                'url' => '#',
            ];
        }
        return [
            'prev' => $prev,
            'next' => $next,
        ];
    }

}