<?php

namespace app\admin\model\product;

use think\Model;


class Product extends Model
{

    

    

    // 表名
    protected $name = 'product';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_open_text',
        'added_time_text'
    ];
    

    
    public function getIsOpenList()
    {
        return ['1' => __('Is_open 1'), '2' => __('Is_open 2')];
    }


    public function getIsOpenTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_open']) ? $data['is_open'] : '');
        $list = $this->getIsOpenList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getAddedTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['added_time']) ? $data['added_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setAddedTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
