<?php

return [
    'Web_menu_id' => 'ID',
    'Language_id' => '语言',
    'Name'        => '菜单名',
    'Pid'         => '父菜单',
    'Url'         => '跳转的url',
    'Add child menu'  => 'إضافة قائمة فرعية',
    'Add'         => 'إضافة',
    'Delete'         => 'حذف',
];
