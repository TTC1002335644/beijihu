<?php

return [
    'Comment'     => 'ID',
    'Language_id' => '语言',
    'Name'        => '姓名',
    'Tel'         => '电话',
    'Email'       => '邮箱',
    'Qq'          => 'QQ',
    'Title'       => '反馈标题',
    'Content'     => '反馈内容'
];
