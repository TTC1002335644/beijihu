<?php

return [
    'Description_id'  => '描述id',
    'News_id'         => '新闻ID',
    'Content'         => '新闻内容',
    'Seo_title'       => 'SEO标题',
    'Seo_keywork'     => 'SEO关键字',
    'Seo_description' => 'SEO描述',
    'Createtime'      => '上传内容时间',
    'Updatetime'      => '更新内容时间'
];
