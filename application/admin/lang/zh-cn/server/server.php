<?php

return [
    'Id'          => 'ID',
    'Language_id' => '语言',
    'Qrcode'      => '二维码',
    'Qq'          => '客服QQ',
    'Createtime'  => '创建时间',
    'Updatetime'  => '更新时间'
];
