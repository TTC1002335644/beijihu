<?php

return [
    'Description_id'  => '详情ID',
    'Base_id'         => '基地ID',
    'Content'         => '内容',
    'Seo_title'       => 'SEO标题',
    'Seo_keywork'     => 'SEO关键字',
    'Seo_description' => 'SEO描述'
];
