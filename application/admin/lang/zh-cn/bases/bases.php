<?php

return [
    'Base_id'          => '基地ID',
    'Language_id'      => '语言',
    'Base_category_id' => '所属分类',
    'Title'            => '基地标题',
    'Image'            => '封面',
    'Views'            => '浏览数',
    'Createtime'       => '上传时间',
    'Updatetime'       => '更新时间'
];
