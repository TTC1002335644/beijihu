<?php

return [
    'Description_id'  => '详情ID',
    'Product_id'      => '产品ID',
    'Content'         => '详情',
    'Seo_title'       => 'SEO标题',
    'Seo_keywork'     => 'SEO关键字',
    'Seo_description' => 'SEO描述'
];
