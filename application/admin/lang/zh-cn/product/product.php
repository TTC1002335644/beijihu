<?php

return [
    'Product_id'       => '产品ID',
    'Language_id'      => '语言',
    'Producy_category' => '0',
    'Title'            => '产品名',
    'Attribute'        => '其他参数',
    'Views'            => '点击次数',
    'Is_open'          => '状态',
    'Is_open 1'        => '可见',
    'Is_open 2'        => '不可见',
    'Added_time'       => '上架时间',
    'Createtime'       => '创建时间',
    'Updatetime'       => '更新时间'
];
