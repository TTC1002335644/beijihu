<?php

return [
    'Id'            => 'ID',
    'Logo'          => 'logo',
    'Favicon'       => '网站小图标',
    'Record_number' => '备案号',
    'Record_url'    => '备案跳转的链接',
    'Contact'       => '联系人',
    'Telphone'      => '手机',
    'Phone'         => '电话',
    'Mail'          => '邮箱',
    'Address'       => '地址'
];
