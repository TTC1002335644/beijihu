<?php

namespace app\admin\controller\news;

use app\common\controller\Backend;
use think\Exception;

/**
 * 新闻内容
 *
 * @icon fa fa-circle-o
 */
class NewsDescription extends Backend
{
    
    /**
     * NewsDescription模型对象
     * @var \app\admin\model\news\NewsDescription
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\news\NewsDescription;

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 添加新闻详情
     * @param int $news_id
     * @param string $content
     * @param string $seo_title
     * @param string $seo_keywork
     * @param string $seo_description
     */
    public function addNewsDescription(int $news_id , string $content = '' , string $seo_title = '' , string $seo_keywork = '' , string $seo_description = ''){
        try{
            $params = [
                'news_id' => $news_id,
                'content' => $content,
                'seo_title' => $seo_title,
                'seo_keywork' => $seo_keywork,
                'seo_description' => $seo_description,
            ];
            $result = $this->model->allowField(true)->save($params);
        }catch (Exception $e){
            $this->error($e->getMessage());
        }
    }

    /**
     * 更新新闻详情，没有则添加
     * @param int $news_id
     * @param string $content
     * @param string $seo_title
     * @param string $seo_keywork
     * @param string $seo_description
     */
    public function updateNewsDescription(int $news_id , string $content = '' , string $seo_title = '' , string $seo_keywork = '' , string $seo_description = ''){
        try{
            $row = $this->model->get(['news_id' => $news_id]);
            if (!$row) {
                $this->addNewsDescription($news_id , $content , $seo_title , $seo_keywork , $seo_description );
            }else{
                $params = [
                    'news_id' => $news_id,
                    'content' => $content,
                    'seo_title' => $seo_title,
                    'seo_keywork' => $seo_keywork,
                    'seo_description' => $seo_description,
                ];
                $result = $row->allowField(true)->save($params);
            }

        }catch (Exception $e){
            $this->error($e->getMessage());
        }
    }

}
