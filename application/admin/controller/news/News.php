<?php

namespace app\admin\controller\news;

use app\common\controller\Backend;
use app\admin\model\news\News as NewsModel;
use think\Db;
use think\Exception;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 新闻关联
 *
 * @icon fa fa-circle-o
 */
class News extends Backend
{
    
    /**
     * News模型对象
     * @var \app\admin\model\news\News
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new NewsModel();
//        $this->model = new \app\admin\model\news\News;
        $this->view->assign("isOpenList", $this->model->getIsOpenList());
        $this->view->assign("isExternalList", $this->model->getIsExternalList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where($where)
                ->where(['language_id' => $this->paramLanguageId])
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where(['language_id' => $this->paramLanguageId])
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }


    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $params['language_id'] = $this->paramLanguageId;
                    if($params['is_external'] == NewsModel::IS_EXTERNAL_TRUE){
                        $params['content'] = '';
                    }else{
                        $params['external_url'] = '';
                    }
                    $result = $this->model->allowField(true)->save($params);

                    $news_id = $this->model->news_id;

                    //添加新闻详情
                    if($result !== false){
                        $seo_title = (isset($params['seo_title']) && !empty($params['seo_title'])) ? $params['seo_title'] : $params['title'];
                        $seo_keywork = (isset($params['seo_keywork']) && !empty($params['seo_keywork'])) ? $params['seo_keywork'] : $seo_title;
                        $seo_description = (isset($params['seo_description']) && !empty($params['seo_description'])) ? $params['seo_description'] : $seo_title;
                        $content = $params['content'];
                        (new NewsDescription())->addNewsDescription($news_id , $content , $seo_title , $seo_keywork , $seo_description);
                    }
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign('language_id' , $this->paramLanguageId);
        return $this->view->fetch();
    }


    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        //获取关联数据
        $with = [
            'NewsDescription'
        ];
        $row = $this->model->get($ids , $with);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }

                    if($params['is_external'] == NewsModel::IS_EXTERNAL_TRUE){
                        $params['content'] = '';
                    }else{
                        $params['external_url'] = '';
                    }
                    $news_id = $row->news_id;

                    unset($params['language_id']);
                    $result = $row->allowField(true)->save($params);

                    if($result !== false){
//                        $news_id = $ids;
                        $seo_title = (isset($params['seo_title']) && !empty($params['seo_title'])) ? $params['seo_title'] : $params['title'];
                        $seo_keywork = (isset($params['seo_keywork']) && !empty($params['seo_keywork'])) ? $params['seo_keywork'] : $seo_title;
                        $seo_description = (isset($params['seo_description']) && !empty($params['seo_description'])) ? $params['seo_description'] : $seo_title;
                        $content = $params['content'];
                        (new NewsDescription())->updateNewsDescription($news_id , $content , $seo_title , $seo_keywork , $seo_description);
                    }


                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
//        dump($row->toArray());
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

}
