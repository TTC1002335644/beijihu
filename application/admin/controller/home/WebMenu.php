<?php

namespace app\admin\controller\home;

use app\common\controller\Backend;
use think\Db;

/**
 * 官网菜单管理
 *
 * @icon fa fa-circle-o
 */
class WebMenu extends Backend
{
    
    /**
     * WebMenu模型对象
     * @var \app\admin\model\home\WebMenu
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\home\WebMenu;

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    public function add(){
        $this->error(__('No rows were inserted'));
    }
    public function edit($ids = null){
        $this->error(__('No rows were updated'));
    }
    public function del($ids = ""){
        $this->error(__('No rows were deleted'));
    }

    public function getList(){
        //设置过滤方法
        if ($this->request->isAjax()) {
            $list = $this->model
                ->where('language_id' , $this->paramLanguageId)
                ->order('web_menu_id', 'asc')
                ->select();
            $list = collection($list)->toArray();
            if(!empty($list)){
                $list = Tree($list,'web_menu_id' , 'pid' , 'child');
            }
            return json(['code' => 1 , 'msg' => __('Save success') , 'lang' => $this->paramLanguageId , "rows" => $list]);
        }
    }

    /**
     * 添加菜单
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function addMenu(){
        $data = json_decode(input('data') , true);
        $languageId = $this->paramLanguageId;
        db('web_menu')->where(['language_id' => $languageId])->delete();
        if(!empty($data)){
            $this->addMenuCommon($languageId , 0 , $data);
        }
        return json(['code' => 1 , 'msg' => __('Save success')]);
    }

    /**
     * 死循环添加菜单
     * @param int $languageId
     * @param int $pid
     * @param $data
     */
    private function addMenuCommon(int $languageId , int $pid , $data){
        if(!empty($data)){
            foreach ($data as $k => $v){
                $param = [
                    'language_id' => $languageId,
                    'name' => $v['name'],
                    'url' => $v['url'],
                    'pid' => $pid
                ];
//                $this->model->save($param);
                $childPid = db('web_menu')->insertGetId($param);
                $this->addMenuCommon($languageId , $childPid , $v['child']);
            }
        }
    }
    

}
