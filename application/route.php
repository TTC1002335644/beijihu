<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use \think\Route;

Route::get('about','index/index/about');//公司简介
Route::get('album','index/index/album');//成功案例
Route::get('album_detail','index/index/album_detail');//成功案例
Route::get('contact','index/index/contact');//联系我们
Route::get('comment','index/index/comment');//在线留言
Route::get('news','index/index/article');//新闻列表
Route::get('news_detail','index/Index/article_detail');//新闻详情
Route::get('product','index/Index/product');//产品列表
Route::get('product_detail','index/Index/product_detail');//产品详情


Route::get('login','/admin/index/login');


return [
    //别名配置,别名只能是映射到控制器且访问时必须加上请求的方法
    '__alias__'   => [
    ],
    //变量规则
    '__pattern__' => [
    ],
//        域名绑定到模块
//        '__domain__'  => [
//            'admin' => 'admin',
//            'api'   => 'api',
//        ],
];
